import React from 'react';
import toJson from 'enzyme-to-json';
import { MemoryRouter } from "react-router-dom";
import { mount, shallow } from "enzyme";

import ToDoItem from "./ToDoItem";

describe('ToDoItem component test' , () => {
    let testListItem;
    let mockOnMoveUp;
    let mockOnMoveDown;
    let mockOnToggle;

    beforeEach(() => {
        testListItem = {
            description: "description",
            id: "2",
            order: 0,
            completed: true
        };
        mockOnMoveUp = jest.fn();
        mockOnMoveDown = jest.fn();
        mockOnToggle = jest.fn();
    });

    it('should render a placeholder', () => {
        const component = mount(
            <MemoryRouter keyLength={0}>
                <ToDoItem
                    listItem={testListItem}
                    listId="1"
                    onMoveDown={mockOnMoveDown}
                    onMoveUp={mockOnMoveUp}
                    onToggle={mockOnToggle}
                />
            </MemoryRouter>
        );
        expect(toJson(component)).toMatchSnapshot();
    });

    it("calls the onToggle function when the CheckBox is changed", () => {
        const wrapper = shallow(<ToDoItem
            listItem={testListItem}
            listId="1"
            onMoveDown={mockOnMoveDown}
            onMoveUp={mockOnMoveUp}
            onToggle={mockOnToggle}
        />);
        wrapper.find("CheckBox").simulate("change");
        expect(mockOnToggle).toHaveBeenCalled();
    });

    it("calls the mockOnMoveUp function when the Up button is clicked", () => {
        const wrapper = shallow(<ToDoItem
            listItem={testListItem}
            listId="1"
            onMoveDown={mockOnMoveDown}
            onMoveUp={mockOnMoveUp}
            onToggle={mockOnToggle}
        />);
        wrapper.find(".up-button").simulate("click");
        expect(mockOnMoveUp).toHaveBeenCalled();
    });

    it("calls the mockOnMoveDown function when the Down button is clicked", () => {
        const wrapper = shallow(<ToDoItem
            listItem={testListItem}
            listId="1"
            onMoveDown={mockOnMoveDown}
            onMoveUp={mockOnMoveUp}
            onToggle={mockOnToggle}
        />);
        wrapper.find(".down-button").simulate("click");
        expect(mockOnMoveDown).toHaveBeenCalled();
    });
});

