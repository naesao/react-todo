// @flow
import React from 'react';
import { getListDeletePath, getListEditPath } from "../pathConfig";
import type {ListItem} from "../models"
import CheckBox from "./CheckBox";
import { Link } from "react-router-dom";

type Props = {
    listItem: ListItem,
    onToggle: (listId: ?string, itemId: ?string) => any,
    onMoveUp: (listId: ?string, itemId: ?string) => any,
    onMoveDown: (listId: ?string, itemId: ?string) => any,
    listId: ?string
}

class ToDoItem extends React.Component<Props> {

    onToggleCheckBox = () => {
        this.props.onToggle(this.props.listId, this.props.listItem.id);
    };

    onMoveUpClick = () => {
        this.props.onMoveUp(this.props.listId, this.props.listItem.id);
    };

    onMoveDownClick = () => {
        this.props.onMoveDown(this.props.listId, this.props.listItem.id);
    };

    render() {
        const {listId, listItem} = this.props;
        const finishedColor = listItem.completed ? "positive" : "";

        return (
            <tr className={`ui attached segment ${finishedColor}`}>
                <td>
                    <CheckBox completed={listItem.completed} onChange={this.onToggleCheckBox}/>
                </td>
                <td>
                    <h5 style={{ textDecoration: listItem.completed ? 'line-through' : 'none' }}>{listItem.description}</h5>
                </td>
                <td className="right floated right aligned content">
                    <button className="ui basic icon button up-button" onClick={this.onMoveUpClick}>
                        <i className="angle double up icon"/>
                    </button>
                    <button className="ui basic icon button down-button" onClick={this.onMoveDownClick}>
                        <i className="angle double down icon"/>
                    </button>
                    <Link to={getListEditPath(listId, listItem.id)} className="ui button primary">
                        Edit
                    </Link>
                    <Link to={getListDeletePath(listId, listItem.id)} className="ui button negative">
                        Delete
                    </Link>
                </td>
            </tr>
        )
    }
}

export default ToDoItem;

