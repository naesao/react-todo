// @flow
import React from 'react';
import { Link } from "react-router-dom";
import { getHomePath } from "../pathConfig";

const Header = () => {
    return (
        <div className="ui pointing menu">
            <Link to={getHomePath()} className="header item">To-Do For You</Link>
            <div className="right menu">
                <Link to={getHomePath()} className="item">Create New List</Link>
            </div>
        </div>
    )
};

export default Header;