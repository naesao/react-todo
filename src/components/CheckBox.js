// @flow
import React from 'react';

type Props = {
    completed: boolean,
    onChange: () => any
}

const CheckBox = (props:Props) => {
    return (
        <div className="ui checkbox">
            <input type="checkbox" name="todo" value={props.completed} checked={props.completed} onChange={props.onChange} />
            <label/>
        </div>
    )
};

CheckBox.defaultProps = {
    completed: false
};

export default CheckBox;