// @flow
import * as React from 'react';
import ReactDOM from 'react-dom';
import ConnectedErrorMessage from "../features/ConnectedErrorMessage";

type Props = {
    title: string,
    content: string,
    actions: Object,
    onDismiss: () => any
}

const Modal = (props: Props) => {
    const modal =  document.querySelector('#modal');
    if (modal===null) return "";
    return ReactDOM.createPortal(
        <div className="ui dimmer modals visiable active" onClick={props.onDismiss}>
            <div className="ui standard modal visible active" onClick={(e) => e.stopPropagation()}>
                <div className="header">
                    {props.title}
                </div>
                <div className="content">
                    {props.content}
                    <ConnectedErrorMessage />
                </div>
                <div className="actions">
                    {props.actions}
                </div>
            </div>
        </div>,
        modal
    )
};

export default Modal;