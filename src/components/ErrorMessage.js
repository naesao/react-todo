// @flow
import React from 'react';

type Props = {
    showError: boolean
}

const ErrorMessage = (props:Props) => {
    if(props.showError){
        return (
            <div className="ui error message">
                Sorry, something went wrong. Please try again later
            </div>
        )
    }
    return ""
};

export default ErrorMessage;