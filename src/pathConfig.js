export const LIST_PATH = "/list/";

export const getHomePath = () => "/";
export const getListPath = (listId) => LIST_PATH + listId + "/";
export const getListCreatePath = (listId) => getListPath(listId) + "new";
export const getListEditPath = (listId, itemId) => getListPath(listId) + "edit/" + itemId;
export const getListDeletePath = (listId, itemId) => getListPath(listId) + "delete/" + itemId;

