import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
    GET_NEW_LIST_ID_ACTIONS,
    GET_NEW_LIST_ID_FAILED,
    GET_NEW_LIST_ID_STARTED,
    GET_NEW_LIST_ID_SUCCEEDED
} from "./types";
import { async } from "./asyncAction";

const mockStore = configureMockStore([thunk]);

describe("Async Action", () => {
    let store;

    beforeEach(() => {
        store = mockStore({});
    });

    it("dispatches async action and returns data on success", async () => {
        const expectedListId = "123";

        const getListId = () => {
            return {
                data: { listId: expectedListId }
            };
        };

        await store.dispatch(async(GET_NEW_LIST_ID_ACTIONS, getListId));
        const actions = store.getActions();

        expect.assertions(3);
        expect(actions[0].type).toEqual(GET_NEW_LIST_ID_STARTED);
        expect(actions[1].type).toEqual(GET_NEW_LIST_ID_SUCCEEDED);
        expect(actions[1].payload.listId).toEqual(expectedListId);
    });

    it("dispatches async action and returns an error", async () => {
        const expectedErrorMessage = "Something bad happened";
        const getListId = () =>
                    Promise.reject({
                        error: expectedErrorMessage
                    });

        await store.dispatch(async(GET_NEW_LIST_ID_ACTIONS, getListId));
        const actions = store.getActions();

        expect.assertions(3);
        expect(actions[0].type).toEqual(GET_NEW_LIST_ID_STARTED);
        expect(actions[1].type).toEqual(GET_NEW_LIST_ID_FAILED);
        expect(actions[1].error.error).toEqual(expectedErrorMessage);
    });
});