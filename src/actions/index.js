import history from "../history";
import _ from 'lodash';

import { getListPath } from "../pathConfig";
import { async, SUCCEEDED } from './asyncAction';
import { deleteListItem, getListId, getListItems, postListItem, putListItem } from "../services/listService";
import { selectListItem } from "../reducers/listReducer";
import { getIdDown, getIdUp } from "../services/reorderService";
import {
    CREATE_LIST_ITEM_ACTIONS,
    DELETE_LIST_ITEM_ACTIONS,
    EDIT_LIST_ITEM_ACTIONS,
    FETCH_LIST_ITEMS_ACTIONS,
    GET_NEW_LIST_ID_ACTIONS,
    MOVE_DOWN_UI_IDS,
    MOVE_UP_UI_IDS
} from "./types";

export const createToDoList = () => async (dispatch, getState) => {
    await dispatch(async(GET_NEW_LIST_ID_ACTIONS, getListId));

    redirectToListWith(getState().lists.listId, getState().ui.status)
};

export const createListItem = (formValues, listId) => async (dispatch, getState) => {
    const maxOrderOfListItems = _.chain(Object.values(getState().lists.listItems)).map('order').max().value();
    formValues["completed"] = false;
    formValues["order"] = _.isNil(maxOrderOfListItems) ? 0: maxOrderOfListItems + 1;
    await dispatch(async(CREATE_LIST_ITEM_ACTIONS, postListItem.bind(this, listId, formValues)));

    redirectToListWith(listId, getState().ui.status)
};

export const fetchListItems = (listId) => {
    return async(FETCH_LIST_ITEMS_ACTIONS, getListItems.bind(this, listId));
};

export const editListItem = (listId, itemId, formValues) => async (dispatch, getState) => {
    const updatedListItem = {...getState().lists.listItems[itemId], ...formValues};
    await dispatch(async(EDIT_LIST_ITEM_ACTIONS, putListItem.bind(this, listId, itemId, updatedListItem)));

    redirectToListWith(listId, getState().ui.status)
};

export const toggleListItem = (listId, itemId) => async (dispatch, getState) => {
    const currentListItem = selectListItem(getState(), itemId);
    currentListItem["completed"] = !currentListItem.completed;
    dispatch(editListItem(listId, itemId, currentListItem));
};

export const moveUpUiListItemIds = (listId, itemId) => {
    return {
        type: MOVE_UP_UI_IDS,
        payload: itemId
    };
};

export const moveDownUiListItemIds = (listId, itemId) =>{
    return {
        type: MOVE_DOWN_UI_IDS,
        payload: itemId
    };
};

export const moveUpListItem = (listId, itemId) => async (dispatch, getState) => {
    const currentListItem = selectListItem(getState(), itemId);
    const currentListItemOrder = currentListItem.order;

    const displacedListItemId = getIdUp(getState().ui.listItems, itemId);
    const displacedListItem = selectListItem(getState(), displacedListItemId);
    const displacedListItemOrder = displacedListItem.order;

    currentListItem["order"] = displacedListItemOrder;
    displacedListItem["order"] = currentListItemOrder;

    dispatch(editListItem(listId, itemId, currentListItem));
    dispatch(editListItem(listId, displacedListItemId, displacedListItem));
    dispatch(moveUpUiListItemIds(listId, itemId));
};

export const moveDownListItem = (listId, itemId) => async (dispatch, getState) => {
    const currentListItem = selectListItem(getState(), itemId);
    const currentListItemOrder = currentListItem.order;

    const displacedListItemId = getIdDown(getState().ui.listItems, itemId);
    const displacedListItem = selectListItem(getState(), displacedListItemId);
    const displacedListItemOrder = displacedListItem.order;

    currentListItem["order"] = displacedListItemOrder;
    displacedListItem["order"] = currentListItemOrder;

    dispatch(editListItem(listId, itemId, currentListItem));
    dispatch(editListItem(listId, displacedListItemId, displacedListItem));
    dispatch(moveDownUiListItemIds(listId,itemId));
};

export const removeListItem = (listId, itemId) => async (dispatch, getState) => {
    await dispatch(async(DELETE_LIST_ITEM_ACTIONS, deleteListItem.bind(this, listId, itemId), itemId));

    redirectToListWith(listId, getState().ui.status)
};

const redirectToListWith = (listId, status) => {
    if (status === SUCCEEDED) {
        history.push(getListPath(listId));
    }
};