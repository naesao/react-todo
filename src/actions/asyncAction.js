// @flow
import type { APIActionTypes } from "./types";

export const UNSTARTED = 'UNSTARTED';
export const STARTED = 'STARTED';
export const SUCCEEDED = 'SUCCEEDED';
export const FAILED = 'FAILED';

export type Status = typeof UNSTARTED | typeof STARTED | typeof SUCCEEDED | typeof FAILED;

interface StartedAsyncAction<T> {
    type: T;
    status: typeof STARTED;
}

interface SucceededAsyncAction<T, P = any> {
    type: T;
    status: typeof SUCCEEDED;
    payload: P;
}

interface FailedAsyncAction<T> {
    type: T;
    status: typeof FAILED;
    error: Error;
}

function startedAsyncAction<T>(type: T): StartedAsyncAction<T> {
    return {
        type,
        status: STARTED,
    };
}

function succeededAsyncAction<T, P>(type: T, payload: P): SucceededAsyncAction<T, P> {
    return {
        type,
        status: SUCCEEDED,
        payload: payload,
    };
}

function failedAsyncAction<T>(type: T, error: Error): FailedAsyncAction<T> {
    return {
        type,
        status: FAILED,
        error,
    };
}

export type AsyncAction<T, P = any> = StartedAsyncAction<T> | SucceededAsyncAction<T, P> | FailedAsyncAction<T>;

export function async(
    type: APIActionTypes,
    api: (...args: any[]) => any,
    payloadOverride:any) {
    return async (dispatch: any) => {
        dispatch(startedAsyncAction(type["started"]));
        try {
            const response = await api();
            const payload = payloadOverride ? payloadOverride: response.data;
            dispatch(succeededAsyncAction(type["succeeded"], payload));
        } catch (e) {
            dispatch(failedAsyncAction(type["failed"], e));
        }
    };
}