// @flow

export type ListItem = {
    description: string,
    id: string,
    order: number,
    completed: boolean
}
