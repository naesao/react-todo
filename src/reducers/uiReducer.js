import {
    GET_NEW_LIST_ID_SUCCEEDED,
    GET_NEW_LIST_ID_STARTED,
    GET_NEW_LIST_ID_FAILED,
    FETCH_LIST_ITEMS_STARTED,
    FETCH_LIST_ITEMS_SUCCEEDED,
    FETCH_LIST_ITEMS_FAILED,
    DELETE_LIST_ITEM_STARTED,
    DELETE_LIST_ITEM_SUCCEEDED,
    DELETE_LIST_ITEM_FAILED,
    CREATE_LIST_ITEM_STARTED,
    CREATE_LIST_ITEM_FAILED,
    CREATE_LIST_ITEM_SUCCEEDED,
    MOVE_UP_UI_IDS,
    MOVE_DOWN_UI_IDS
} from "../actions/types";
import { FAILED, STARTED, SUCCEEDED, UNSTARTED } from "../actions/asyncAction";
import type { Status, AsyncAction } from "../actions/asyncAction";
import _ from "lodash";
import { moveDown, moveUp } from "../services/reorderService";

export interface UiState {
    status: Status,
    listItems: number[],
    errorMessage?: ?string
}

const initialState: UiState = {
    status: UNSTARTED,
    listItems: []
};

export default (state: UiState = initialState, action: AsyncAction<any, any>): UiState => {
    switch (action.type) {
        case GET_NEW_LIST_ID_STARTED:
        case FETCH_LIST_ITEMS_STARTED:
        case DELETE_LIST_ITEM_STARTED:
        case CREATE_LIST_ITEM_STARTED: {
            return { ...state, status: STARTED };
        }
        case GET_NEW_LIST_ID_FAILED:
        case FETCH_LIST_ITEMS_FAILED:
        case DELETE_LIST_ITEM_FAILED:
        case CREATE_LIST_ITEM_FAILED: {
            return { ...state, status: FAILED, errorMessage: action.error.message }
        }
        case GET_NEW_LIST_ID_SUCCEEDED: {
            return { ...state, status: SUCCEEDED, listItems: [] }
        }
        case FETCH_LIST_ITEMS_SUCCEEDED: {
            const orderedItems = _.orderBy(action.payload, 'order');
            const newListItems = [..._.map(orderedItems, 'id')];
            return { ...state, status: SUCCEEDED, listItems: newListItems }
        }
        case DELETE_LIST_ITEM_SUCCEEDED: {
            const newListItems = _.without(state.listItems, action.payload, 10);
            return { ...state, status: SUCCEEDED, listItems: newListItems }
        }
        case CREATE_LIST_ITEM_SUCCEEDED: {
            const newListItems = [...state.listItems, action.payload.id];
            return { ...state, status: SUCCEEDED, listItems: newListItems };
        }
        case MOVE_UP_UI_IDS: {
            const newListItems = moveUp(state.listItems, action.payload);
            return { ...state, listItems: newListItems };
        }
        case MOVE_DOWN_UI_IDS: {
            const newListItems = moveDown(state.listItems, action.payload);
            return { ...state, listItems: newListItems };
        }
        default:
            return state;
    }
}

export const selectListItems = (state) => state.ui.listItems.map((noteId) => state.lists.listItems[noteId]);
export const showErrorMessage = (state) => state.ui.status === FAILED;
export const isLoadingStatus = (state) => state.ui.status === STARTED;
