import _ from 'lodash';

import {
    CREATE_LIST_ITEM_SUCCEEDED,
    FETCH_LIST_ITEMS_SUCCEEDED,
    DELETE_LIST_ITEM_SUCCEEDED,
    GET_NEW_LIST_ID_SUCCEEDED
} from "../actions/types";

export default (state = { listId: null, listItems: {} }, action) => {
    switch (action.type) {
        case GET_NEW_LIST_ID_SUCCEEDED: {
            const listId = action.payload ? action.payload.listId : null;
            return { ...state, listId, listItems: {} };
        }
        case FETCH_LIST_ITEMS_SUCCEEDED: {
            const newListItems = { ...state.listItems, ..._.mapKeys(action.payload, 'id') };
            return { ...state, listItems: newListItems };
        }
        case CREATE_LIST_ITEM_SUCCEEDED: {
            const newListItems = { ...state.listItems, [action.payload.id]: action.payload };
            return { ...state, listItems: newListItems };
        }
        case DELETE_LIST_ITEM_SUCCEEDED: {
            const newListItems = _.omit(state.listItems, action.payload);
            return { ...state, listItems: newListItems };
        }
        default:
            return state;
    }
}

export const selectListItem = (state, itemId) => state.lists.listItems[itemId];
