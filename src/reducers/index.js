import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import listReducer from './listReducer';
import uiReducer from "./uiReducer";

export default combineReducers({
    form: formReducer,
    lists: listReducer,
    ui: uiReducer
});