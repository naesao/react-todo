import listReducer from "./listReducer";
import {
    CREATE_LIST_ITEM_SUCCEEDED,
    DELETE_LIST_ITEM_SUCCEEDED,
    FETCH_LIST_ITEMS_SUCCEEDED,
    GET_NEW_LIST_ID_SUCCEEDED
} from "../actions/types";
import { SUCCEEDED } from "../actions/asyncAction";

describe('list reducer test', () => {
    const initialState = { listId: null, listItems: {} };
    const listItemId1 = "1";
    const listItemId2 = "2";

    const listItem1 = {
        "description": "1",
        "completed": true,
        "order": 2,
        "id": listItemId1
    };
    const listItem2 = {
        "description": "2",
        "completed": false,
        "order": 1,
        "id": listItemId2
    };

    test('returns the initial state', () => {
        const action = {};
        expect(listReducer(undefined, action)).toEqual(initialState);
    });

    test('returns a new list id with GET_NEW_LIST_ID_SUCCEEDED', () => {
        const listId = "123";
        const action = { type: GET_NEW_LIST_ID_SUCCEEDED, status: SUCCEEDED, payload: {"listId": listId}};
        const expectedState = { listId: listId, listItems: {} };
        expect(listReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns a list of items with FETCH_LIST_ITEMS_SUCCEEDED', () => {
        const payload = [listItem1, listItem2];

        const action = { type: FETCH_LIST_ITEMS_SUCCEEDED, status: SUCCEEDED, payload: payload};
        const expectedState = { listId: null, listItems: {[listItemId1]: listItem1, [listItemId2]: listItem2} };
        expect(listReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns a list with a new item with CREATE_LIST_ITEM_SUCCEEDED', () => {
        const payload = listItem1;

        const action = { type: CREATE_LIST_ITEM_SUCCEEDED, status: SUCCEEDED, payload: payload};
        const expectedState = { listId: null, listItems: {[listItemId1]: listItem1} };
        expect(listReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns a list with item removed with DELETE_LIST_ITEM_SUCCEEDED', () => {
        const oldState = { listId: null, listItems: {[listItemId1]: listItem1, [listItemId2]: listItem2} };
        const payload = "1";

        const action = { type: DELETE_LIST_ITEM_SUCCEEDED, status: SUCCEEDED, payload: payload};
        const expectedState = { listId: null, listItems: {[listItemId2]: listItem2} };
        expect(listReducer(oldState, action)).toEqual(expectedState);
    });
});