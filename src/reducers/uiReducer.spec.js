import {
    CREATE_LIST_ITEM_SUCCEEDED,
    DELETE_LIST_ITEM_SUCCEEDED,
    FETCH_LIST_ITEMS_SUCCEEDED, GET_NEW_LIST_ID_FAILED, GET_NEW_LIST_ID_STARTED,
    GET_NEW_LIST_ID_SUCCEEDED, MOVE_DOWN_UI_IDS, MOVE_UP_UI_IDS
} from "../actions/types";
import { FAILED, STARTED, SUCCEEDED, UNSTARTED } from "../actions/asyncAction";
import uiReducer from "./uiReducer";

describe('ui reducer test', () => {
    const initialState =  {
        status: UNSTARTED,
        listItems: []
    };
    const listItemId1 = "1";
    const listItemId2 = "2";

    const listItem1 = {
        "description": "1",
        "completed": true,
        "order": 2,
        "id": listItemId1
    };
    const listItem2 = {
        "description": "2",
        "completed": false,
        "order": 1,
        "id": listItemId2
    };

    it('returns the initial state', () => {
        const action = {};
        expect(uiReducer(undefined, action)).toEqual(initialState);
    });

    test('sets status to STARTED if GET_NEW_LIST_ID_STARTED', () => {
        const action = { type: GET_NEW_LIST_ID_STARTED, status: STARTED};
        const expectedState = { status: STARTED, listItems: [] };
        expect(uiReducer(initialState, action)).toEqual(expectedState);
    });

    test('sets status to FAILED with error message if GET_NEW_LIST_ID_FAILED', () => {
        const errorMessage = "errorrrr"
        const action = { type: GET_NEW_LIST_ID_FAILED, status: FAILED,  error : {message: errorMessage}};
        const expectedState = { status: FAILED, listItems: [], errorMessage: errorMessage };
        expect(uiReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns a status of SUCCEEDED with GET_NEW_LIST_ID_SUCCEEDED', () => {
        const action = { type: GET_NEW_LIST_ID_SUCCEEDED, status: SUCCEEDED, payload: {"listId": "123"}};
        const expectedState = { status: SUCCEEDED, listItems: [] };
        expect(uiReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns an itemIds list with FETCH_LIST_ITEMS_SUCCEEDED', () => {
        const payload = [listItem1, listItem2];

        const action = { type: FETCH_LIST_ITEMS_SUCCEEDED, status: SUCCEEDED, payload: payload};
        const expectedState = { status: SUCCEEDED, listItems: [listItemId2, listItemId1] };
        expect(uiReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns an itemIds list with a new item with CREATE_LIST_ITEM_SUCCEEDED', () => {
        const payload = listItem1;

        const action = { type: CREATE_LIST_ITEM_SUCCEEDED, status: SUCCEEDED, payload: payload};
        const expectedState = { status: SUCCEEDED, listItems: [listItemId1] };
        expect(uiReducer(initialState, action)).toEqual(expectedState);
    });

    test('returns an itemIds list with item removed with DELETE_LIST_ITEM_SUCCEEDED', () => {
        const oldState = { status: STARTED, listItems: [listItemId2, listItemId1] };
        const payload = "1";

        const action = { type: DELETE_LIST_ITEM_SUCCEEDED, status: SUCCEEDED, payload: payload};
        const expectedState = { status: SUCCEEDED, listItems: [listItemId2] };
        expect(uiReducer(oldState, action)).toEqual(expectedState);
    });

    test('returns an itemIds list with new order with MOVE_UP_UI_IDS', () => {
        const oldState = { status: null, listItems: [listItemId2, listItemId1] };
        const payload = "1";

        const action = { type: MOVE_UP_UI_IDS, status: UNSTARTED, payload: payload};
        const expectedState = { status: null, listItems: [listItemId1, listItemId2] };
        expect(uiReducer(oldState, action)).toEqual(expectedState);
    });

    test('returns an itemIds list with new order with MOVE_DOWN_UI_IDS', () => {
        const oldState = { status: null, listItems: [listItemId2, listItemId1] };
        const payload = "2";

        const action = { type: MOVE_DOWN_UI_IDS, status: UNSTARTED, payload: payload};
        const expectedState = { status: null, listItems: [listItemId1, listItemId2] };
        expect(uiReducer(oldState, action)).toEqual(expectedState);
    });
});