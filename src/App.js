import React from 'react';
import { Router, Route, Switch } from "react-router-dom";
import history from './history';

import Home from "./features/Home";
import Header from "./components/Header";
import ListItemCreate from "./features/lists/ListItemCreate";
import ListItemEdit from "./features/lists/ListItemEdit";
import ToDoList from "./features/lists/ToDoList";
import ListItemDelete from "./features/lists/ListItemDelete";
import ConnectedErrorMesage from "./features/ConnectedErrorMessage";
import {
getHomePath,
getListCreatePath,
getListDeletePath,
getListEditPath,
getListPath,
} from "./pathConfig";

const App = () => {
    return (
        <div>
            <Router history={history}>
                <div>
                    <Header/>
                    <div className="ui container">
                        <Switch>
                            <Route path={getHomePath()} exact component={Home}/>
                            <Route path={getListPath(":listId")} exact component={ToDoList}/>
                            <Route path={getListCreatePath(":listId")} exact component={ListItemCreate}/>
                            <Route path={getListEditPath(":listId",":itemId")} exact component={ListItemEdit}/>
                            <Route path={getListDeletePath(":listId",":itemId")} exact component={ListItemDelete}/>
                        </Switch>
                        <ConnectedErrorMesage />
                    </div>
                </div>
            </Router>
        </div>
    )
};

export default App;