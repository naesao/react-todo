import ErrorMessage from "../components/ErrorMessage";
import { showErrorMessage } from "../reducers/uiReducer";
import { connect } from "react-redux";

const mapStateToProp = state =>{
    return {showError: showErrorMessage(state) }
};

export default connect(mapStateToProp)(ErrorMessage);