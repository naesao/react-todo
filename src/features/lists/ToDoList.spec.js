import React from "react";
import {shallow } from "enzyme";

import { ToDoList } from "./ToDoList";

describe('ToDoList component test' , () => {
    let testListItems;
    let fetchListItems;
    let toggleListItem;
    let moveUpListItem;
    let moveDownListItem;
    let params;

    beforeEach(() => {
        testListItems = [{
            description: "description1",
            id: "1",
            order: 0,
            completed: false
        }, {
            description: "description2",
            id: "2",
            order: 1,
            completed: true
        }];
        fetchListItems = jest.fn();
        toggleListItem = jest.fn();
        moveUpListItem = jest.fn();
        moveDownListItem = jest.fn();
        params = { params: { listId: "10"}}
    });

    it("contains multiple ToDoItem components", () => {
        const wrapper = shallow(<ToDoList
            listItems={testListItems}
            fetchListItems={fetchListItems}
            toggleListItem={toggleListItem}
            moveUpListItem={moveUpListItem}
            moveDownListItem={moveDownListItem}
            match={params}
        />);
        // console.log(wrapper.debug());
        expect(wrapper).toContainMatchingElements(2, 'ToDoItem')
    });
});

