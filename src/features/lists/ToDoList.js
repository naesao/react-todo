// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import type { Match } from "react-router-dom";
import _ from "lodash";

import ToDoItem from "../../components/ToDoItem";
import { fetchListItems, moveDownListItem, moveUpListItem, toggleListItem } from "../../actions";
import { getListCreatePath } from "../../pathConfig";
import { selectListItems } from "../../reducers/uiReducer";
import type { ListItem } from "../../models/index";

type Props = {
    listItems: ListItem[],
    fetchListItems: (listId: ?string) => any,
    toggleListItem: (listId: ?string, itemId: ?string) => any,
    moveUpListItem: (listId: ?string, itemId: ?string) => any,
    moveDownListItem: (listId: ?string, itemId: ?string) => any,
    match: Match
}

export class ToDoList extends React.Component<Props> {

    componentDidMount() {
        this.props.fetchListItems(this.props.match.params.listId)
    }

    renderList() {
        const { listId } = this.props.match.params;
        return _.map(this.props.listItems, (listItem => {
            return <ToDoItem
                listId={listId}
                listItem={listItem}
                onToggle={this.props.toggleListItem}
                onMoveUp={this.props.moveUpListItem}
                onMoveDown={this.props.moveDownListItem}
                key={listItem.id} />
        }));
    }

    render() {
        return (
            <div>
                <div className="ui top attached message">
                    <h2>To-Do's</h2>
                </div>
                <table className="ui bottom attached table">
                    <tbody>
                    {this.renderList()}
                    </tbody>
                </table>
                <Link to={getListCreatePath(this.props.match.params.listId)} className="ui button primary">
                    Create To-Do Item
                </Link>
            </div>)
    }
}

const mapStateToProps = (state) => {
    return { listItems: selectListItems(state) }
};

const mapDispatchToProps = { fetchListItems, toggleListItem, moveUpListItem, moveDownListItem };

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);