// @flow
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import type { Match } from "react-router-dom";

import ListItemForm from "./ListItemForm";
import { fetchListItems, editListItem } from "../../actions";
import { selectListItem } from "../../reducers/listReducer";
import type { ListItem } from "../../models/index";

type Props = {
    listItem: ListItem,
    onSubmit: (formValues: ListItem) => any,
    match: Match,
    fetchListItems: (listId: ?string) => any,
    editListItem: (listId: ?string,itemId: ?string, formValues:Object) => any
};

class ListItemEdit extends React.Component<Props> {

    componentDidMount() {
        this.props.fetchListItems(this.props.match.params.listId)
    }

    onSubmit = (formValues: ListItem) => {
        this.props.editListItem(this.props.match.params.listId, this.props.match.params.itemId, formValues);
    };

    render() {
        if (!this.props.listItem) {
            return "loading.."
        }
        return (
            <div>
                <div>
                    <h3>Edit List Item</h3>
                    <ListItemForm
                        initialValues={_.pick(this.props.listItem, 'description')}
                        onSubmit={this.onSubmit}
                    />
                </div>
            </div>)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        listItem: selectListItem(state, ownProps.match.params.itemId)
    }
};

export default connect(mapStateToProps, { fetchListItems, editListItem })(ListItemEdit);