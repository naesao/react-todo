// @flow
import * as React from 'react';
import { reduxForm, Field } from "redux-form";
import type { FormProps } from 'redux-form';
import type { FieldProps } from 'redux-form';

import LoadingPrimaryButton from "../LoadingPrimaryButton";
import type { ListItem } from "../../models/index";

type Props = {
    onSubmit: (formValues: ListItem) => any,
} & FormProps;

class ListItemForm extends React.Component<Props> {

    renderError = ({ error, touched }) => {
        if (touched && error) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            )
        }
    };

    renderInput = ({ input, label, meta }: {label: string} & FieldProps ): React.Node => {
        const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
        return (
            <div className={className}>
                <label>{label}</label>
                <input {...input} autoComplete="off"/>
                {this.renderError(meta)}
            </div>
        )
    };

    onSubmit = (formValues) => {
        this.props.onSubmit(formValues);
    };

    render() {
        return (
            <form className="ui form error" onSubmit={this.props.handleSubmit(this.onSubmit)}>
                <Field name="description" component={this.renderInput} label="What do you want to do?"/>
                <LoadingPrimaryButton>Submit</LoadingPrimaryButton>
            </form>
        )
    }
}

const validate = formValues => {
    const errors = {};

    if (!formValues.description) {
        errors.description = 'you must enter a description';
    }

    return errors;
};

export default reduxForm({ form: 'listItemForm', validate })(ListItemForm);