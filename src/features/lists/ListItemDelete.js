// @flow
import React from 'react';
import history from "../../history";
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import type { Match } from "react-router-dom";

import LoadingPrimaryButton from "../LoadingPrimaryButton";
import Modal from "../../components/Modal";
import { fetchListItems, removeListItem } from "../../actions";
import { getListPath } from "../../pathConfig";
import { selectListItem } from "../../reducers/listReducer";
import type { ListItem } from "../../models/index";

type Props = {
    fetchListItems: (listId: ?string) => any,
    removeListItem: (listId: ?string, itemId: ?string) => any,
    match: Match,
    listItem: ListItem
}

class ListItemDelete extends React.Component<Props> {
    componentDidMount() {
        this.props.fetchListItems(this.props.match.params.listId)
    }

    renderActions() {
        const itemId = this.props.match.params.itemId;
        const listId = this.props.match.params.listId;
        return (
            <React.Fragment>
                <LoadingPrimaryButton onClick={() => this.props.removeListItem(listId, itemId)}>
                    Delete
                </LoadingPrimaryButton>
                <Link to={getListPath(listId)} className="ui button">Cancel</Link>
            </React.Fragment>
        )
    }

    renderContent() {
        if (!this.props.listItem) {
            return "Are you sure you want to delete this list item?"
        }
        return `Are you sure you want to delete the list item with description: ${this.props.listItem.description}?`
    }

    render() {
        if (!this.props.listItem) {
            return <div>Loading...</div>
        }
        return (
            <Modal
                title="Delete List Item"
                content={this.renderContent()}
                actions={this.renderActions()}
                onDismiss={() => history.push(getListPath(this.props.match.params.listId))}
            />
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        listItem: selectListItem(state, ownProps.match.params.itemId)
    }
};

export default connect(mapStateToProps, { fetchListItems, removeListItem })(ListItemDelete);