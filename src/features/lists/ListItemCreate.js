// @flow
import React from 'react';
import { connect } from 'react-redux';
import type { Match } from "react-router-dom";

import ListItemForm from "./ListItemForm";
import { createListItem, fetchListItems } from "../../actions";
import type { ListItem } from "../../models/index";

type Props = {
    fetchListItems: (listId: ?string) => any,
    createListItem: (formValues: ListItem, listId: ?string) => any,
    match: Match
}
class ListItemCreate extends React.Component<Props> {

    componentDidMount() {
        this.props.fetchListItems(this.props.match.params.listId)
    }

    onSubmit = (formValues: ListItem) => {
        this.props.createListItem(formValues, this.props.match.params.listId);
    };

    render() {
        return (
            <div>
                <h3>Create To-Do Item</h3>
                <ListItemForm onSubmit={this.onSubmit} />
            </div>
        )
    }
}

export default connect(null, { createListItem, fetchListItems })(ListItemCreate);