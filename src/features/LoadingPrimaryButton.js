// @flow
import React from 'react';
import { isLoadingStatus } from "../reducers/uiReducer";
import { connect } from "react-redux";

type Props = {
    loading: boolean,
    onClick: () => any,
    children: any
}

const LoadingPrimaryButton = (props:Props) => {
    const showLoading = (loading: boolean) => { return loading ? 'loading': ''};
    const {loading, children, onClick} = props;
    return (
        <button className={`ui primary button ${showLoading(loading)}`} onClick={onClick}>{children}</button>
    )
};

const mapStateToProps = (state) => {
    return {
        loading: isLoadingStatus(state)
    }
};

export default connect(mapStateToProps)(LoadingPrimaryButton);