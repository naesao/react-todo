// @flow
import React from "react";
import { connect } from "react-redux";

import { createToDoList } from "../actions";
import LoadingPrimaryButton from "./LoadingPrimaryButton";

type Props = {
    createToDoList: () => any
}
class Home extends React.Component<Props> {
    render() {
        return (
                <div style={{ textAlign: 'center' }}>
                    <LoadingPrimaryButton onClick={this.props.createToDoList}>
                        Create To-Do List
                    </LoadingPrimaryButton>
                </div>
        )
    }
}

export default connect(null, {createToDoList})(Home);