import { moveItem, moveUp, moveDown, getIdDown, getIdUp } from './reorderService'

describe('reorderService moveItem', () => {
    it('move item up', () => {
        const oldList = [1,2];
        const updatedList = moveItem(oldList, 0, 1);
        expect(updatedList).toEqual([2,1])
    });
    it('move item down', () => {
        const oldList = [1,2,3];
        const updatedList = moveItem(oldList, 2, 1);
        expect(updatedList).toEqual([1,3,2])
    });
});

describe('reorderService moveUp', () => {
    it('move item up when item in middle', () => {
        const oldList = [1,2,3];
        const updatedList = moveUp(oldList, 2);
        expect(updatedList).toEqual([2,1,3])
    });
    it('move item up when item in start', () => {
        const oldList = [1,2,3];
        const updatedList = moveUp(oldList, 1);
        expect(updatedList).toEqual([1,2,3])
    });
    it('move item up when item in end', () => {
        const oldList = [1,2,3];
        const updatedList = moveUp(oldList, 3);
        expect(updatedList).toEqual([1,3,2])
    });
});

describe('reorderService moveDown', () => {
    it('move item down when item in middle', () => {
        const oldList = [1,2,3];
        const updatedList = moveDown(oldList, 2);
        expect(updatedList).toEqual([1,3,2])
    });
    it('move item down when item in start', () => {
        const oldList = [1,2,3];
        const updatedList = moveDown(oldList, 1);
        expect(updatedList).toEqual([2,1,3])
    });
    it('move item down when item in end', () => {
        const oldList = [1,2,3];
        const updatedList = moveDown(oldList, 3);
        expect(updatedList).toEqual([1,2,3])
    });
});

describe('reorderService getIdDown', () => {
    it('get item down when item in middle', () => {
        const oldList = [1,2,3];
        const updatedList = getIdDown(oldList, 2);
        expect(updatedList).toEqual(3)
    });
    it('get item down when item in start', () => {
        const oldList = [1,2,3];
        const updatedList = getIdDown(oldList, 1);
        expect(updatedList).toEqual(2)
    });
    it('get item down when item in end', () => {
        const oldList = [1,2,3];
        const updatedList = getIdDown(oldList, 3);
        expect(updatedList).toEqual(3)
    });
});

describe('reorderService getIdUp', () => {
    it('get item up when item in middle', () => {
        const oldList = [1,2,3];
        const updatedList = getIdUp(oldList, 2);
        expect(updatedList).toEqual(1)
    });
    it('get item up when item in start', () => {
        const oldList = [1,2,3];
        const updatedList = getIdUp(oldList, 1);
        expect(updatedList).toEqual(1)
    });
    it('get item up when item in end', () => {
        const oldList = [1,2,3];
        const updatedList = getIdUp(oldList, 3);
        expect(updatedList).toEqual(2)
    });
});

