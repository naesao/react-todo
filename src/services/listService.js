import work180api from "./apis/work180api";

export const getListId = () => work180api.get('/get-list-id');
export const getListItems = (listId) => work180api.get(`/items/${listId}`);
export const postListItem = (listId, formValues) => work180api.post(`/items/${listId}`, formValues);
export const putListItem = (listId, itemId, formValues) => work180api.put(`/items/${listId}/${itemId}`, formValues);
export const deleteListItem = (listId, itemId) => work180api.delete(`/items/${listId}/${itemId}`);