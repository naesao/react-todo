import axios from 'axios';

export default axios.create({
    baseURL: 'http://api.staging.work180.co/api/v1/todo-list'
});