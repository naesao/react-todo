export const moveItem = (data, from, to) => {
    const newList = [...data];
    const f = newList.splice(from, 1)[0];
    newList.splice(to, 0, f);
    return newList;
};

export const moveUp = (data, idToMoveUp) => {
    const currentIndex = data.findIndex(x => x === idToMoveUp);
    const newIndex = currentIndex === 0 ? 0 : currentIndex - 1;
    return moveItem(data, currentIndex, newIndex);
};

export const moveDown = (data, idToMoveDown) => {
    const currentIndex = data.findIndex(x => x === idToMoveDown);
    const maxIndex = data.length - 1;
    const newIndex = currentIndex === maxIndex ? maxIndex : currentIndex + 1;
    return moveItem(data, currentIndex, newIndex);
};

export const getIdDown = (listItemIds, idToMoveDown) => {
    const currentIndex = listItemIds.findIndex(x => x === idToMoveDown);
    const maxIndex = listItemIds.length - 1;
    const newIndex = currentIndex === maxIndex ? maxIndex : currentIndex + 1;
    return listItemIds[newIndex];
};

export const getIdUp = (listItemIds, idToMoveUp) => {
    const currentIndex = listItemIds.findIndex(x => x === idToMoveUp);
    const newIndex = currentIndex === 0 ? 0 : currentIndex - 1;
    return listItemIds[newIndex];
};