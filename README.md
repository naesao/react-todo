## To Do App

Hello!

This is a basic "To Do App" using React, Redux, React-Router, & Semantic UI.
It hits the Work180 Staging API and has the following features:

* Create a new todo list
* Create a todo list item
* Mark an item as complete
* Edit the item description
* Delete an item
* Re-order the list


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

